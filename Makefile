# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jeboisne <jeboisne@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2024/11/11 12:53:39 by jeboisne          #+#    #+#              #
#    Updated: 2024/11/11 19:19:22 by jeboisne         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

## VARIABLES 

NAME	= libft.a
CC 		= cc
CFLAGS = -Wall -Wextra -Werror

SRCS_FILES	=	ft_isalpha.c ft_isdigit.c ft_isalnum.c ft_isascii.c ft_isprint.c \
				ft_strlen.c ft_memset.c ft_bzero.c ft_memcpy.c ft_strlcpy.c \
				ft_strlcat.c ft_toupper.c ft_tolower.c	ft_strchr.c ft_strrchr.c \
				ft_strncmp.c ft_memchr.c ft_memcmp.c ft_strnstr.c ft_atoi.c ft_calloc.c \
				ft_strdup.c ft_memmove.c \
				ft_substr.c ft_strjoin.c ft_strtrim.c ft_split.c	\
				ft_itoa.c ft_strmapi.c ft_striteri.c ft_putchar_fd.c ft_putstr_fd.c \
				ft_putendl_fd.c ft_putnbr_fd.c

SRC_DIR		= ./src/

SRCS = ${addprefix ${SRC_DIR}, ${SRCS_FILES}}

BONUS_FILE	=	ft_lstnew.c ft_lstadd_front.c ft_lstsize.c ft_lstlast.c ft_lstadd_back.c \
 			ft_lstdelone.c ft_lstclear.c ft_lstiter.c

BONUS = ${addprefix ${SRC_DIR}, ${BONUS_FILE}}

OBJS	= ${SRCS:.c=.o}

OBJS_BONUS = ${BONUS:.c=.o}

HEADERS	= ./


## RULES 

all : ${NAME}

${NAME} : ${OBJS}
	ar rcs ${NAME} ${OBJS}

%.o	: %.c
	${CC} ${CFLAGS} -I ${HEADERS} -c $< -o $@

bonus : $(OBJS) $(OBJS_BONUS)
	ar rcs $(NAME) $(OBJS) $(OBJS_BONUS)

clean :
	rm -rf ${OBJS} ${OBJS_BONUS}

fclean : clean
	rm -rf ${NAME}

re : fclean all

.PHONY = all bonus clean fclean re 